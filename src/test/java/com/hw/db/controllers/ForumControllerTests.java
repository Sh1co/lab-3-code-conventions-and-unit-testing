package com.hw.db.controllers;

import org.junit.jupiter.api.*;
import com.hw.db.models.Forum;
import com.hw.db.models.User;

class ForumControllerTests {
    private User loggedIn;
    private Forum toCreate;

    @BeforeEach
    @DisplayName("forum creation test")
    void createForumTest() {
        loggedIn = new User("some","some@email.mu", "name", "nothing");
        toCreate = new Forum(12, "some", 3, "title", "some");
    }

    @Test
    @DisplayName("Correct forum creation test")
    void correctlyCreatesForum() {
        Assertions.assertEquals("abc", "abc");
    }
}


